$("#filterBtn").click(function() {
  $("#filters").addClass("show");
  $('.scrollpane').jScrollPane();
});
$(".closeFilters").click(function(){
  $("#filters").removeClass("show");
});
var filterCounter = 0;
$("#filterLists li").click(function() {
  if ($(this).hasClass("selected")) {
    $(this).removeClass("selected");
    filterCounter = filterCounter - 1;
  } else {
    $(this).addClass("selected");
    filterCounter = filterCounter + 1;
  }
  if (filterCounter>0) {
    $(".applyBtn").removeClass("disabled");
  } else {
    $(".applyBtn").addClass("disabled");
  }
});
var products = $('.product');
$(".clearBtn").click(function() {
  if ($("#filterLists li").hasClass("selected")) {
    $("#filterLists li").removeClass("selected");
    $(".applyBtn").addClass("disabled");
    $("#filters").removeClass("show");
    $("#filterBtn .filtercount").text("");
    filterCounter = 0;
    products.show();
  }
});

// filter search
$('.applyBtn').click(function() {
  $("#filters").removeClass("show");

    products.hide();
    var ulType = "";
    var customType = "";
    var productIdArray = [];
    $(".selected").each(function() {
      customType = $( this ).data('filter');
      ulType = $(this).parent().data('type');
      products
          .filter(function () {
              if (jQuery.inArray( $(this).data('id'), productIdArray ) == -1) {
                if ($(this).data(ulType).indexOf(customType) != -1) {
                  productIdArray.push($(this).data('id'));
                }
                return $(this).data(ulType).indexOf(customType) != -1;
              }
          })
          .show();
    });
    var leadingZero = "";
    if (filterCounter<10 && filterCounter != 0) {
      leadingZero = "0";
    }
    $("#filterBtn .filtercount").text(leadingZero+filterCounter);

});


// Organize product showcasing with different layouts
function footerOrganizer(tableAllignment,rowNo,rowClassRemove1,rowClassRemove2,productSize) {

  var rowHover = "";
  var productClass = "product columns to-load onscreen ready";
  if (rowNo == 4) {
    productSize = " large-3 medium-4 small-6";
  } else {
    productSize = " large-"+productSize+" medium-"+productSize+" small-"+productSize;
  }
  productClass = productClass + productSize;


  $("#catalog").attr("class","collectionview " + tableAllignment + " show menu-nav");
  $(".product").attr("class",productClass);
  $("li .row" + rowNo).attr("class","items row" + rowNo + "" + rowHover + " active");
  $("li .row" + rowClassRemove1).removeClass("active");
  $("li .row" + rowClassRemove2).removeClass("active");
}

$("li .row1").click(function() {
  footerOrganizer("rows",1,2,4,12);
});
$("li .row2").click(function() {
  footerOrganizer("thumbs",2,1,4,6);
});
$("li .row4").click(function() {
  footerOrganizer("small-thumbs",4,1,2,"");
});
