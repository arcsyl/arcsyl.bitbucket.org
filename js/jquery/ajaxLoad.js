
// this should be the Ajax Method.
// and load the url content
var setCurrentPage = function(url,title) {
  $('#js-page-view').empty();
$('#js-page-view').load((url + "  #" + title) || "/");
$.getScript('js/jquery/singleJquery.js');
$.getScript('js/filter.js');
$.getScript('js/jquery/ajaxLoad.js');


$(".menu-nav a[href='" + url + "']").fadeTo(500, 0.3);
};

$('.menu-nav a').click(function(e){
e.preventDefault();
var targetUrl = $(this).attr('href'),
  targetTitle = $(this).attr('title');

$(".menu-nav a[href='" + window.location.pathname + "']").fadeTo(500, 1.0);

window.history.pushState({url: "" + targetUrl + ""}, targetTitle, targetUrl);
// jQuery('#js-page-view').html('');
// $('#js-page-view').empty();
setCurrentPage(targetUrl,targetTitle);
// $.getScript('js/jquery/ajaxLoad.js');
// $.getScript('js/jquery/singleJquery.js');
});

window.onpopstate = function(e) {
$(".menu-nav a").fadeTo('fast', 1.0);
setCurrentPage(e.state ? e.state.url : null);
};


_.templateSettings = {
interpolate : /\{\{(.+?)\}\}/g
};